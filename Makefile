#
# This is a GNU Makefile.
#

MASTER = td-kaya
STYS = $(wildcard *.sty)

all: $(MASTER).pdf $(MASTER)-enonce.pdf $(MASTER)-corrige.pdf

.PHONY: all clean PHONY

clean:
	rm -f $(MASTER).aux $(MASTER)-corrige.aux $(MASTER)-enonce.aux

%.pdf: %.tex $(STYS) gitHeadLocal.gin
	latexmk -pdflua $<

$(MASTER)-%.tex: $(MASTER).tex
	sed 's/\\usepackage\(\[.*\]\)\?{TD}/\\usepackage[$*]{TD}/' < $< > $@

gitHeadLocal.gin: PHONY
	./scripts/script-git-info > $@

