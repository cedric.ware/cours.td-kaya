# TD Équation de Kaya

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/2.0/fr/"><img alt="Licence CC-BY-NC-SA" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/2.0/fr/88x31.png" /></a>

| Dernière version | Version en développement |
|------------------|--------------------------|
| v2.2 \[&nbsp;[énoncé](https://gitlab.telecom-paris.fr/cedric.ware/cours.td-kaya/-/jobs/artifacts/v2.2/file/td-kaya-enonce.pdf?job=build-release)&nbsp;\|&nbsp;[corrigé](https://gitlab.telecom-paris.fr/cedric.ware/cours.td-kaya/-/jobs/artifacts/v2.2/file/td-kaya-corrige.pdf?job=build-release)&nbsp;\|&nbsp;[version prof](https://gitlab.telecom-paris.fr/cedric.ware/cours.td-kaya/-/jobs/artifacts/v2.2/file/td-kaya.pdf?job=build-release)&nbsp;\]| Branche principale :<br> \[&nbsp;[énoncé](https://gitlab.telecom-paris.fr/cedric.ware/cours.td-kaya/-/jobs/artifacts/master/file/td-kaya-enonce.pdf?job=build-master)&nbsp;\|&nbsp;[corrigé](https://gitlab.telecom-paris.fr/cedric.ware/cours.td-kaya/-/jobs/artifacts/master/file/td-kaya-corrige.pdf?job=build-master)&nbsp;\|&nbsp;[version prof](https://gitlab.telecom-paris.fr/cedric.ware/cours.td-kaya/-/jobs/artifacts/master/file/td-kaya.pdf?job=build-master)&nbsp;\]|

<details>
<summary>Versions précédentes</summary>
<ul>
<li>v2.0 (Donnée le 2023-01-19 à Télécom Paris) [&nbsp;[énoncé](https://gitlab.telecom-paris.fr/cedric.ware/cours.td-kaya/-/jobs/artifacts/v2.0/file/td-kaya-enonce.pdf?job=build-release)&nbsp;|&nbsp;[corrigé](https://gitlab.telecom-paris.fr/cedric.ware/cours.td-kaya/-/jobs/artifacts/v2.0/file/td-kaya-corrige.pdf?job=build-release)&nbsp;|&nbsp;[version prof](https://gitlab.telecom-paris.fr/cedric.ware/cours.td-kaya/-/jobs/artifacts/v2.0/file/td-kaya.pdf?job=build-release)&nbsp;]</li>
<li>v1.0 (Donnée le 2021-09-08 à Télécom Paris) [&nbsp;[énoncé](https://gitlab.telecom-paris.fr/cedric.ware/cours.td-kaya/-/jobs/artifacts/v1.0/file/td-kaya-enonce.pdf?job=build-release)&nbsp;|&nbsp;[corrigé](https://gitlab.telecom-paris.fr/cedric.ware/cours.td-kaya/-/jobs/artifacts/v1.0/file/td-kaya-corrige.pdf?job=build-release)&nbsp;|&nbsp;[version prof](https://gitlab.telecom-paris.fr/cedric.ware/cours.td-kaya/-/jobs/artifacts/v1.0/file/td-kaya.pdf?job=build-release)&nbsp;]</li>
<li>v0.9 (test) [&nbsp;[énoncé](https://gitlab.telecom-paris.fr/cedric.ware/cours.td-kaya/-/jobs/artifacts/v0.9/file/td-kaya-enonce.pdf?job=build-release)&nbsp;|&nbsp;[corrigé](https://gitlab.telecom-paris.fr/cedric.ware/cours.td-kaya/-/jobs/artifacts/v0.9/file/td-kaya-corrige.pdf?job=build-release)&nbsp;|&nbsp;[version prof](https://gitlab.telecom-paris.fr/cedric.ware/cours.td-kaya/-/jobs/artifacts/v0.9/file/td-kaya.pdf?job=build-release)&nbsp;]</li>
</li>
</details>
